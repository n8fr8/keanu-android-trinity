This is not intended for regular users. It is a demonstration and test application for developers and others interested in our work on the Matrix.org protocol.

Learn more at https://matrix.org