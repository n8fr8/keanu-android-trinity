package info.guardianproject.keanu.core.ui.friends

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.MainActivity
import info.guardianproject.keanuapp.databinding.FragmentFriendsListBinding

class FriendsListFragment : Fragment() {

    private var mAdapter: FriendsRecyclerViewAdapter? = null

    private var mSearchString: String? = null

    private lateinit var mBinding: FragmentFriendsListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        mBinding = FragmentFriendsListBinding.inflate(inflater, container, false)

        mBinding.emptyView.setOnClickListener {
            (activity as? MainActivity)?.inviteFriend()
        }

        setupRecyclerView(mBinding.recyclerView)

        return mBinding.root
    }

    fun doSearch(searchString: String?) {
        mSearchString = searchString
    }

    val friendCount: Int
        get() = mAdapter?.itemCount ?: 1

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)

        mAdapter =
            FriendsRecyclerViewAdapter(
                this
            )

        recyclerView.adapter = mAdapter
        if (mAdapter?.itemCount == 0) {
            recyclerView.visibility = View.GONE
            mBinding.emptyView.visibility = View.VISIBLE
        }
        else {
            recyclerView.visibility = View.VISIBLE
            mBinding.emptyView.visibility = View.GONE
        }
    }

    fun updateListVisibility() {
        if (mAdapter?.itemCount == 0) {
            mBinding.recyclerView.visibility = View.GONE
            mBinding.emptyView.visibility = View.VISIBLE
        }
        else if (mBinding.recyclerView.visibility == View.GONE) {
            mBinding.recyclerView.visibility = View.VISIBLE
            mBinding.emptyView.visibility = View.GONE
        }
    }
}