package info.guardianproject.keanu.core.ui.quickresponse


import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import info.guardianproject.keanu.core.ui.room.MessageAction
import info.guardianproject.keanu.core.ui.room.RoomViewModel
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentQuickResponseBinding
import org.matrix.android.sdk.api.session.room.model.message.*
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageBody
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import java.net.URLConnection
import java.util.*

class QuickResponseFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentQuickResponseBinding
    private val roomViewModel: RoomViewModel by activityViewModels()
    private var thumbLoaded = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val timeline = roomViewModel.getSelectedMessage()
        binding = FragmentQuickResponseBinding.inflate(layoutInflater)


        binding.emojiRecycleView.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        val emojiAdapter = EmojiAdapter { position -> onEmojiClickListener(position) }

        emojiAdapter.recentEmojiList = RoomViewModel.emojis

        binding.emojiRecycleView.adapter = emojiAdapter

        roomViewModel.attachmentThumbnailUri.observe(this, {
            if (!thumbLoaded) {
                if ((!Uri.EMPTY.equals(it))) {
                    binding.messageImageView.visibility = View.VISIBLE
                    GlideUtils.loadImageFromUri(context, it, binding.messageImageView, true)

                } else {
                    binding.messageContentTextView.visibility = View.VISIBLE
                    binding.messageImageView.visibility = View.GONE
                }
                thumbLoaded = true
            }
        })

        binding.addReactionTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.ADD_REACTION
            roomViewModel.selectedEmoji.value = null
            dismiss()
        }

        binding.replyTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.REPLY
            dismiss()
        }

        binding.copyTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.COPY
            dismiss()
        }

        binding.fwdTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.FORWARD
            dismiss()
        }

        binding.deleteTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.DELETE
            dismiss()
        }
        binding.downloadTextView.setOnClickListener {
            roomViewModel.messageAction.value = MessageAction.DOWNLOAD
            dismiss()
        }

        if (timeline != null) {
            populateResponseView(timeline)
        }

        return binding.root
    }

    private fun onEmojiClickListener(position: Int) {
        roomViewModel.selectedEmoji.value = RoomViewModel.emojis[position]
        roomViewModel.messageAction.value = MessageAction.QUICK_REACTION

        dismiss()
    }

    private fun populateResponseView(timeline: TimelineEvent) {
        val timestamp = timeline.root.originServerTs
        val senderName =
            if (timeline.senderInfo.displayName.isNullOrBlank()) timeline.senderInfo.userId
            else timeline.senderInfo.displayName

        binding.messageSenderTextView.text = senderName

        if (timestamp != null) {
            binding.messageTimeTextView.text = PrettyTime.format(Date(timestamp), requireContext())
        }
        else {
            binding.messageContentTextView.text = "-:-"
        }

        val msg = timeline.getLastMessageBody()
        binding.messageContentTextView.text = if (msg?.startsWith(">") == true) {
            msg.split("\n\n")[1]
        }
        else {
            msg
        }

        when (val messageContent = timeline.getLastMessageContent()) {
            is MessageAudioContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.GONE
                binding.messageTypeTextView.text = messageContent.audioInfo?.size?.let {
                    "${RoomViewModel.getFileSizeInKb(it)} kB"
                }
                binding.copyTextView.visibility = View.GONE
                binding.downloadTextView.visibility = View.VISIBLE
            }

            is MessageImageContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE

                val info = messageContent.info
                binding.messageTypeTextView.text = if (info != null) {
                    "${info.width} x ${info.height} - ${RoomViewModel.getFileSizeInKb(info.size)} kB"
                }
                else {
                    "- x - - - kB"
                }
                binding.copyTextView.visibility = View.GONE

                binding.downloadTextView.visibility = View.VISIBLE
                roomViewModel.getSelectedMessageImageUri(messageContent = messageContent)
            }

            is MessageStickerContent -> {
                binding.messageTypeTextView.visibility = View.GONE
                binding.messageImageView.visibility = View.VISIBLE
                roomViewModel.getSelectedMessageImageUri(messageContent = messageContent)
                binding.copyTextView.visibility = View.GONE
                binding.downloadTextView.visibility = View.GONE
            }

            is MessageVideoContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE

                val info = messageContent.videoInfo
                binding.messageTypeTextView.text = if (info != null) {
                    "${info.width} x ${info.height} - ${RoomViewModel.getFileSizeInKb(info.size)} kB"
                }
                else {
                    "- x - - - kB"
                }
                binding.copyTextView.visibility = View.GONE
                binding.downloadTextView.visibility = View.VISIBLE

                roomViewModel.getSelectedMessageImageUri(messageContent = messageContent)
            }
            is MessageFileContent -> {
                binding.messageTypeTextView.visibility = View.VISIBLE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE

                val mimeType = getMimeType(messageContent.mimeType, messageContent.body)
                setAttachmentImage(mimeType)

                val info = messageContent.info
                binding.messageTypeTextView.text = if (info != null) {
                    "${RoomViewModel.getFileSizeInKb(info.size)} kB"
                }
                else {
                    "- kB"
                }
                binding.copyTextView.visibility = View.GONE
                binding.downloadTextView.visibility = View.VISIBLE
            }

            is MessageWithAttachmentContent -> {
                binding.messageTypeTextView.visibility = View.GONE
                binding.messageContentTextView.visibility = View.VISIBLE
                binding.messageImageView.visibility = View.VISIBLE

                setAttachmentImage(getMimeType(messageContent.mimeType, messageContent.body))
                binding.copyTextView.visibility = View.GONE
                binding.downloadTextView.visibility = View.VISIBLE
            }

            else -> {
                binding.messageTypeTextView.visibility = View.GONE
                binding.messageContentTextView.visibility = View.GONE
                binding.messageImageView.visibility = View.VISIBLE

                roomViewModel.getSelectedImageContent(timeline.getLastMessageBody(), context)
                binding.copyTextView.visibility = View.VISIBLE
                binding.downloadTextView.visibility = View.GONE
            }
        }
    }

    private fun setAttachmentImage(mimeType: String) {
        when {
            mimeType.isEmpty() -> {
                binding.messageImageView.setImageResource(R.drawable.file_unknown)
            }

            mimeType.contains("html") -> {
                binding.messageImageView.setImageResource(R.drawable.file_unknown)
            }

            mimeType == "text/csv" -> {
                binding.messageImageView.setImageResource(R.drawable.proofmodebadge)
            }

            mimeType.contains("pdf") -> {
                binding.messageImageView.setImageResource(R.drawable.file_pdf)
            }

            mimeType.contains("doc") || mimeType.contains("word") -> {
                binding.messageImageView.setImageResource(R.drawable.file_doc)
            }

            mimeType.contains("zip") -> {
                binding.messageImageView.setImageResource(R.drawable.file_zip)
            }

            else -> {
                binding.messageImageView.setImageResource(R.drawable.file_unknown)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): QuickResponseFragment {
            return QuickResponseFragment()
        }
    }
}

fun getMimeType(mimeType: String?, filename: String?): String {
    if (filename?.isNotEmpty() == true && (mimeType.isNullOrEmpty() || mimeType.startsWith("application"))) {
        val guessed = URLConnection.guessContentTypeFromName(filename)

        if (guessed?.isNotEmpty() == true) {
            return if (guessed == "video/3gpp") "audio/3gpp" else guessed
        }
    }

    return if (mimeType == "video/3gpp") "audio/3gpp" else mimeType ?: ""
}

interface QuickResponseLongClickListener {
    fun onQuickResponseClick(timeline: TimelineEvent, attachment: Uri?, thumbnail: Uri?)
}

