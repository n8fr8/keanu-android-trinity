package info.guardianproject.keanu.core.ui.me

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.PasswordDialogFragment
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.ImageChooserHelper
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentMyProfileBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.user.model.User
import java.io.File

class MyProfileFragment: Fragment(), View.OnClickListener, TextView.OnEditorActionListener {

    private val mApp
        get() = activity?.application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mUser: User?
        get() = mSession?.myUserId?.let { mSession?.getUser(it) }

    private lateinit var mBinding: FragmentMyProfileBinding

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    private var mImageChooserHelper: ImageChooserHelper? = null

    private lateinit var mPickAvatar: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activity = activity ?: return

        val helper = ImageChooserHelper(activity)
        mImageChooserHelper = helper

        mPickAvatar = registerForActivityResult(ActivityResultContracts.StartActivityForResult(),
                helper.getCallback(this::setAvatar))
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mBinding = FragmentMyProfileBinding.inflate(inflater, container, false)

        mSession?.contentUrlResolver()?.resolveThumbnail(mUser?.avatarUrl, 512, 512, ContentUrlResolver.ThumbnailMethod.SCALE)
                ?.let {
                    GlideUtils.loadImageFromUri(activity, Uri.parse(it), mBinding.imgAvatar, false)
        }

        mBinding.imgAvatar.setOnClickListener(this)

        mBinding.etName.setText(mUser?.displayName)
        mBinding.etName.setOnEditorActionListener(this)

        mBinding.tvUserId.text = mSession?.myUserId

        mBinding.imgEditName.setOnClickListener(this)

        mBinding.btChangePassword.setOnClickListener(this)

        mBinding.btViewDevices.setOnClickListener(this)

        mBinding.btLogout.setOnClickListener(this)

        var version: String? = null

        activity?.let {
            try {
                version = it.packageManager?.getPackageInfo(it.packageName, 0)?.versionName
            }
            catch (ignored: PackageManager.NameNotFoundException) {}
        }

        if (version != null) {
            mBinding.tvBuildNumber.text = "${getString(R.string.version_label)}: $version"
        }
        else {
            mBinding.tvBuildNumber.visibility = View.GONE
        }

        return mBinding.root
    }

    override fun onClick(v: View?) {
        val activity = activity ?: return

        when (v) {
            mBinding.imgAvatar -> {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                        Snackbar.make(v, R.string.grant_perms, Snackbar.LENGTH_LONG).show()
                    } else {
                        mRequestCamera.launch(Manifest.permission.CAMERA)
                    }
                } else {
                    mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
                }
            }

            mBinding.imgEditName -> {
                mBinding.etName.requestFocus()
            }

            mBinding.btChangePassword -> {
                PasswordDialogFragment(
                        PasswordDialogFragment.Style.NEW_PASSWORD,
                        getString(R.string.lock_screen_create_passphrase),
                        okLabel = getString(R.string.ok)
                ) { pdf ->
                    val oldPassword = pdf.oldPassword ?: return@PasswordDialogFragment
                    val newPassword = pdf.password ?: return@PasswordDialogFragment

                    mCoroutineScope.launch {
                        mSession?.changePassword(oldPassword, newPassword)
                    }
                }.show(activity.supportFragmentManager, "PASSWORD")
            }

            mBinding.btViewDevices -> {
                startActivity(mApp?.router?.devices(activity))
            }

            mBinding.btLogout -> {
                AlertDialog.Builder(activity)
                    .setTitle(R.string.menu_sign_out)
                    .setMessage(R.string.are_you_sure)
                    .setNeutralButton(android.R.string.cancel, null)
                    .setNegativeButton(R.string.menu_sign_out) { _, _ ->
                        lifecycleScope.launch {
                            mApp?.signOut()

                            activity.startActivity(mApp?.router?.router(activity))

                            activity.finish()
                        }
                    }
                    .show()
            }
        }
    }

    /**
     * Handle name change.
     */
    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        // According to an answer on StackOverflow
        // https://stackoverflow.com/questions/1489852/android-handle-enter-in-an-edittext
        // This is the definitive way to handle all enter key events.
        if (event == null) {
            if (actionId != EditorInfo.IME_ACTION_DONE && actionId != EditorInfo.IME_ACTION_NEXT) return false
        }
        else if (event.action != KeyEvent.ACTION_DOWN) {
            return false
        }
        else if (actionId != EditorInfo.IME_NULL) {
            return false
        }

        v?.clearFocus()

        (activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
                ?.hideSoftInputFromWindow(v?.windowToken, 0)

        val userId = mSession?.myUserId ?: return true
        val name = v?.text?.toString() ?: return true

        mCoroutineScope.launch {
            mSession?.setDisplayName(userId, name)
        }

        return true
    }

    private var mRequestCamera = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
    }

    private fun setAvatar(bmp: Bitmap) {
        mBinding.imgAvatar.setImageDrawable(BitmapDrawable(resources, bmp))

        val userId = mSession?.myUserId ?: return

        mCoroutineScope.launch {
            @Suppress("BlockingMethodInNonBlockingContext")
            val file = File.createTempFile("avatar", "jpg")

            file.outputStream().use { os ->
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, os)
            }

            try {
                mSession?.updateAvatar(userId, Uri.fromFile(file), file.name)
            }
            finally {
                file.delete()
            }
        }
    }
}