package info.guardianproject.keanu.core.util

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.QRCodeWriter

object BitmapHelper {

    fun qrCode(contents: String, width: Int, height: Int): Bitmap {
        val qrWriter = QRCodeWriter()

        val hints = HashMap<EncodeHintType, Any>()
        hints[EncodeHintType.MARGIN] = 0

        val matrix = qrWriter.encode(contents, BarcodeFormat.QR_CODE, width, height, hints)

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)

        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (matrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }

        return bitmap
    }
}
