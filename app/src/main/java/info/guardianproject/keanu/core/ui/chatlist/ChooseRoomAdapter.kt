package info.guardianproject.keanu.core.ui.chatlist

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import info.guardianproject.keanu.core.util.extensions.isArchived
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ChooseRoomItemBinding
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import java.lang.ref.WeakReference
import java.util.*
import java.util.regex.Pattern

class ChooseRoomAdapter(listener: Listener) : RecyclerView.Adapter<ChooseRoomItemHolder>(), Observer<List<RoomSummary>> {

    interface Listener {
        val activity: AppCompatActivity?

        fun updateListVisibility()

        fun onSelected(roomId: String)
    }

    private val mListener = WeakReference(listener)

    private val mActivity
        get() = mListener.get()?.activity

    private val mSession
        get() = (mActivity?.application as? ImApp)?.matrixSession

    private val mTypedValue = TypedValue()
    private val mBackground: Int
    private var mRoomList = listOf<RoomSummary>()
    private var mFilterString: String? = null

    private var mShowLastMessage: Boolean = true

    private val queryParams by lazy {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN, Membership.INVITE)
        builder.build()
    }

    init {
        mActivity?.theme?.resolveAttribute(R.attr.chatBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
        setHasStableIds(true)

        onChanged(mSession?.getRoomSummaries(queryParams))

        mActivity?.let { mSession?.getRoomSummariesLive(queryParams)?.observe(it, this) }
    }

    fun setShowChatMessages (showLastMessage: Boolean) {
        mShowLastMessage = showLastMessage
    }

    fun filter(filterString: String?) {
        mFilterString = filterString

        onChanged(mSession?.getRoomSummaries(queryParams))
    }

    private var showArchived = false
        set(value) {
            field = value

            onChanged(mSession?.getRoomSummaries(queryParams))
        }

    fun get(id: Long) : RoomSummary? {
        return mRoomList.getOrNull(id.toInt())
    }

    override fun onChanged(roomSummaries: List<RoomSummary>?) {

        var newRoomList = roomSummaries?.filter { showArchived == it.isArchived } ?: mutableListOf()

        if (mFilterString?.isNotEmpty() == true) {
            val search = Pattern.compile(Pattern.quote(mFilterString!!), Pattern.CASE_INSENSITIVE)

            newRoomList = newRoomList.filter { search.matcher(it.displayName).find() }
        }

        mRoomList = newRoomList.sortedWith(
                compareByDescending<RoomSummary> { it.membership == Membership.INVITE }
                        .then(compareByDescending {
                            it.latestPreviewableEvent?.root?.originServerTs ?: it.encryptionEventTs
                        }))

        notifyDataSetChanged()

        mListener.get()?.updateListVisibility()
    }

    fun refresh() {
        onChanged(mSession?.getRoomSummaries(queryParams))
    }

    //implement actual stable id map for room summaries
    private var mRoomStableId = HashMap<String,Long>()

    override fun getItemId(position: Int): Long {
        val roomSummary = mRoomList[position]
        var roomId = mRoomStableId[roomSummary.roomId]

        if (roomId == null) {
            roomId = Date().time
            mRoomStableId[roomSummary.roomId] = roomId
        }

        return roomId
    }

    override fun getItemCount(): Int {
        return mRoomList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseRoomItemHolder {
        val binding = ChooseRoomItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.root.setBackgroundResource(mBackground)

        var viewHolder = binding.root.tag as? ChooseRoomItemHolder

        if (viewHolder == null) {
            viewHolder = ChooseRoomItemHolder(binding)
            binding.root.tag = viewHolder
        }

        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: ChooseRoomItemHolder, position: Int) {
        val clItem = viewHolder.itemView as? ChooseRoomItem
        val roomSummary = mRoomList[position]
        val roomId = roomSummary.roomId
        val nickname = roomSummary.displayNameWorkaround
        val avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(roomSummary.avatarUrl, 120, 120, ThumbnailMethod.SCALE)

        clItem?.bind(viewHolder, roomId, nickname, avatarUrl)

        clItem?.setOnClickListener {
            mListener.get()?.onSelected(roomId)
        }
    }


}